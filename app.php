<?php

use TService\Exceptions\Endpoint404Exception;
use TService\Exceptions\MissingParamException;

$app->notFound(function () use ($app) {
	new Endpoint404Exception();
});

$app->get('/list_movies', function () use ($app) {
	/** @var \Phalcon\Http\Request $request */
	$request = $app['request'];
	$limit = $request->get("limit", "int", 20);
	if ($limit < 1 || $limit > 50) {
		throw new MissingParamException("limit");
	}
	$page = $request->get("page", null, 1);
	if ($page < 1) {
		throw new MissingParamException("page");
	}
	$term = $request->get("query_term");
	$genre = $request->get("genre", null, "all");
	$sort_by = strtolower($request->get("sort_by", null, "seeders"));
	$sort_order = strtolower($request->get("order_by", null, "desc"));
	if (!in_array($sort_order, ["desc", "asc"])) {
		throw new MissingParamException("sort_by");
	}
	if (!in_array($sort_by, ["title", "year", "leechers", "seeders"])) {
		throw new MissingParamException("sort_by");
	}
	$data_criteria = [
		"torrents.dead" => 0,
		"tmdb_id" => ['$exists' => true]
	];
	$meta_criteria = [
		'limit' => $limit,
		'skip' => ($page - 1) * $limit
	];
	if ($term) {
		$data_criteria["title"] = [
			'$regex' => $term
		];
	}
	if (strtolower($genre) !== "all") {
		$data_criteria["genres"] = $genre;
	}
	$meta_criteria['$sort'] = [
		(in_array($sort_by, ["leechers", "seeders"])) ?
			"torrents." . $sort_by : $sort_by => $sort_order == "asc" ? 1 : -1
	];
	$movies = Movies::find(array_merge([$data_criteria], $meta_criteria));
	$result = array_map(function ($movie) {
		return $movie->getInYTSFormat();
	}, $movies);
	$core = $app->getDi()->getShared('core');
	$core->buildResponse($result, "movies")->send();
});

$app->get("/movie_details", function () use ($app) {
	/** @var \Phalcon\Http\Request $request */
	$request = $app['request'];
	$id = $request->get("movie_id", "int", null, true);
	if (!$id) {
		throw new MissingParamException("movie_id");
	}
	$movie = Movies::findFirst([
		[
			"_id" => (int)$id,
			"torrents.dead" => 0,
			"tmdb_id" => ['$exists' => true]
		]
	]);
	if (!$movie) {
		throw new \TService\Exceptions\Movie\MovieNotFoundException();
	}
	$core = $app->getDi()->getShared('core');
	$core->buildResponse($movie->getInYTSFormat())->send();
});
