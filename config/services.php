<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Logger;
use Phalcon\Mvc\Url as UrlResolver;

$di = new FactoryDefault();

$di->set('config', $config, true);

$di->set('url', function () use ($config) {
	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);
	return $url;
});

$di->set("core", function () use ($config) {
	$core = new \TService\Core();
	return $core;
});

$di->set('mongo', function () use ($config) {
	$mongo = new MongoClient($config->database->dsn);
	return $mongo->selectDB($config->database->name);
}, true);

$di->set('collectionManager', function () {
	return new Phalcon\Mvc\Collection\Manager();
}, true);
