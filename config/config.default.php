<?php

return new \Phalcon\Config([
	'database' => [
		'dsn' => 'mongodb://localhost:27015',
		'name' => 'tservice',
	],
	'application' => [
		'baseUri' => '/src/',
		'modelsDir' => APP_PATH . '/models/',
		'tserviceDir' => APP_PATH . '/TService/',
		'logsDir' => APP_PATH . '/logs/',
		'urls' => [
			'posters' => "http://cdn.fridaylab.org/posters",
			'backdrops' => "http://cdn.fridaylab.org/backdrops",
			'torrents' => "http://cdn.fridaylab.org/torrents",
		]
	]
]);
