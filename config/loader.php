<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerDirs([
	$config->application->modelsDir,
]);

$loader->registerNamespaces([
	"TService" => $config->application->tserviceDir,
	"TServiceExceptions" => $config->application->tserviceDir . "Exceptions/",
]);

$loader->register();

