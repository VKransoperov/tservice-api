<?php

namespace TService\Exceptions;

use Phalcon\Http\Response;

class BaseException extends \Exception
{

	protected $code = 0;
	protected $message = "";

	public function __construct($args = [])
	{
		$resp = new Response();
		$resp->setContent(json_encode(array_merge([
			"status" => "error",
			"message" => $this->message,
			"code" => $this->code,
		], $args)));
		$resp->setContentType("application/json");
		$resp->send();
	}

}