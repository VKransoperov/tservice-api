<?php

namespace TService\Exceptions;

use Phalcon\Http\Response;

class Endpoint404Exception extends BaseException
{

	protected $code = 1;
	protected $message = "API endpoint does not exist";

}