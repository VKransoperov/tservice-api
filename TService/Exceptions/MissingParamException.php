<?php

namespace TService\Exceptions;

use Phalcon\Http\Response;

class MissingParamException extends BaseException
{

	protected $code = 2;
	protected $message = "Missing a valid input parameter";

	public function __construct($parameter)
	{
		if(is_array($parameter)){
			$key = "parameters";
		}else{
			$key = "parameter";
		}
		parent::__construct([
			$key => $parameter
		]);
	}

}