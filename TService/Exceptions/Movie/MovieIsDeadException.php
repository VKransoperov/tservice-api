<?php

namespace TService\Exceptions\Movie;

use Phalcon\Http\Response;
use TService\Exceptions\BaseException;

class MovieIsDeadException extends BaseException
{

	protected $code = 11;
	protected $message = "Movie has no alive data sources";

}