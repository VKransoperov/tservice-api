<?php

namespace TService\Exceptions\Movie;

use Phalcon\Http\Response;
use TService\Exceptions\BaseException;

class MovieNotFoundException extends BaseException
{

	protected $code = 10;
	protected $message = "Movie not found";

}