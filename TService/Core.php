<?php

namespace TService;

use Phalcon\Http\Response;


class Core
{
	public function buildResponse($data, $key = "data")
	{
		$resp = new Response();
		$resp->setContent(json_encode([
			"status" => "ok",
			$key => $data
		], JSON_UNESCAPED_UNICODE));
		$resp->setContentType("application/json");
		return $resp;
	}
}