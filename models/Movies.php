<?php

use Phalcon\Mvc\Collection;

class Movies extends Collection
{
	public function getSource()
	{
		return "movies";
	}

	public function getTorrents()
	{
		$config = $this->getDI()->getShared("config");
		$torrents = [];
		foreach ($this->torrents as $torrent) {
			if ($torrent["dead"] == 1) {
				continue;
			}
			$totalSize = $size = 0;
			foreach ($torrent["files"] as $file) {
				$totalSize += $file["size"];
				if ($file["type"] == "video" && $size == 0) {
					$size = $file["size"];
				}
			}
			$torrents[] = (object)[
				"magnet" => $torrent["magnet_link"],
				"size" => $totalSize,
				"filesize" => $size,
				"seed" => $torrent["seeders"],
				"peer" => $torrent["seeders"] + $torrent["leechers"],
				"url" => $config["application"]["urls"]["torrents"] . "/" . strtolower($torrent["infohash"]) . ".torrent",
				"quality" => $torrent["quality"]
			];
		}
		return $torrents;
	}

	public function getInYTSFormat(){
		$config = $this->getDI()->getShared("config");
		return (object)[
			"id" => $this->_id,
			"title" => $this->title,
			"runtime" => $this->runtime,
			"certification" => $this->age_rating,
			"year" => $this->year,
			"imdb_id" => $this->imdb_id,
			"tmdb_id" => $this->tmdb_id,
			"image" => $this->cover ? $config["application"]["urls"]["posters"] . $this->cover : null,
			"cover" => $this->cover ? $config["application"]["urls"]["posters"] . $this->cover : null,
			"backdrop" => $this->backdrop ? $config["application"]["urls"]["backdrops"] . $this->backdrop : null,
			"synopsis" => $this->description,
			"torrents" => $this->getTorrents(),
		];
	}

}
