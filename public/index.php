<?php

use Phalcon\Mvc\Micro;

error_reporting(E_ALL);

define('APP_PATH', realpath('..'));


/**
 * Read the configuration
 */
$config = include __DIR__ . "/../config/config.php";

/**
 * Include Services
 */
include APP_PATH . '/config/services.php';

/**
 * Include Autoloader
 */
include APP_PATH . '/config/loader.php';

/**
 * Starting the application
 * Assign service locator to the application
 */
$app = new Micro($di);

/**
 * Incude Application
 */
include APP_PATH . '/app.php';


//$debug = new \Phalcon\Debug();
//$debug->listen();

/**
 * Handle the request
 */
$app->handle();
